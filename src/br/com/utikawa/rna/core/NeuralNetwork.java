package br.com.utikawa.rna.core;

import br.com.utikawa.rna.core.activationFunction.ActivationFunction;
import br.com.utikawa.rna.core.activationFunction.ActivationReLU;

import java.util.ArrayList;

public class NeuralNetwork {
    private ArrayList<NeuralLayer> layers = new ArrayList<>();

    private NeuralNetwork() {
    }

    public NeuralNetwork(int inNeurons, int[] hiddenNeurons, int outNeurons) {
        addLayer(inNeurons, null);
        for(int hiddenNeuron : hiddenNeurons) {
            addLayer(hiddenNeuron);
        }
        addLayer(outNeurons);
    }

    private void addLayer(NeuralLayer layer) {
        if(layers.size() > 0) {
            layers.get(layers.size() - 1).connect(layer);
        }
        layers.add(layer);
    }

    public void addLayer(int neurons) {
        addLayer(neurons, new ActivationReLU());
    }

    public void addLayer(int neurons, ActivationFunction activationFunction) {
        addLayer(new NeuralLayer(neurons, activationFunction));
    }

    public void randomize(int minValue, int maxValue) {
        for(NeuralLayer neuralLayer : layers) {
            neuralLayer.randomize(minValue, maxValue);
        }
    }

    public void mutate(int minValue, int maxValue) {
        for(NeuralLayer neuralLayer : layers) {
            neuralLayer.mutate(minValue, maxValue);
        }
    }

    public long[] feedForward(long[] inputData) {
        return layers.get(0).feedForward(inputData);
    }

    public NeuralNetwork getClone() {
        NeuralNetwork neuralNetwork = new NeuralNetwork();

        for(NeuralLayer item : layers) {
            neuralNetwork.addLayer(item.getClone());
        }

        return neuralNetwork;
    }

    public void print() {
        for(int i=0; i<layers.size(); i++) {
            System.out.println("Camada " + i);
            layers.get(i).print();
        }
    }
}
