package br.com.utikawa.rna.core.neuron;

public class Input implements INeuron {
    private Long value;

    Input(Long value) {
        setValue(value);
    }

    public void setValue(Long value) {
        this.value = value;
    }

    @Override
    public void calculate() {
    }

    @Override
    public Long getOutput() {
        return value;
    }
}
