package br.com.utikawa.rna.core.neuron;

import br.com.utikawa.rna.core.activationFunction.ActivationFunction;

import java.util.ArrayList;
import java.util.List;

class NeuronInput {
    private INeuron neuron;
    private Long weight;

    NeuronInput(INeuron neuron, Long weight) {
        this.neuron = neuron;
        this.weight = weight;
    }

    public Long getInput() {
        return neuron.getOutput() * weight;
    }
}

public class Neuron implements INeuron {
    private Long output = 0L;
    private ArrayList<NeuronInput> inputList = new ArrayList<>();
    private ActivationFunction activationFunction;

    Neuron(ActivationFunction activationFunction) {
        this.activationFunction = activationFunction;
    }

    @Override
    public void calculate() {
        long sum = 0L;
        for(final long value : getInputList()) {
            sum += value;
        }

        output = activationFunction.calculate(sum);
    }

    @Override
    public Long getOutput() {
        return output;
    }

    public List<Long> getInputList() {
        ArrayList<Long> inputs = new ArrayList<>();

        for(NeuronInput neuronInput : inputList) {
            inputs.add(neuronInput.getInput());
        }

        return inputs;
    }

    public void registerInput(Neuron neuron, Long weight) {
        inputList.add(new NeuronInput(neuron, weight));
    }

    public void registerInput(List<NeuronInput> neuronInputList) {
        inputList.addAll(neuronInputList);
    }
}
