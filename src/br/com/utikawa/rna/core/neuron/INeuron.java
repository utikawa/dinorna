package br.com.utikawa.rna.core.neuron;

public interface INeuron {
    void calculate();
    Long getOutput();
}
