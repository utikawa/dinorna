package br.com.utikawa.rna.core.activationFunction;

public interface ActivationFunction {
    long calculate(long value);
}
