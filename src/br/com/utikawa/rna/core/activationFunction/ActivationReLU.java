package br.com.utikawa.rna.core.activationFunction;

public class ActivationReLU implements ActivationFunction {
    @Override
    public long calculate(long value) {
        return Math.max(0, value);
    }
}
