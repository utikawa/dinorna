package br.com.utikawa.rna.core;

import br.com.utikawa.rna.core.activationFunction.ActivationFunction;
import br.com.utikawa.rna.utils.MatrixUtils;

public class NeuralLayer {
    private long[] biasLimits = new long[2];
    private long[] synapseLimits = new long[2];

    private ActivationFunction activationFunction;
    private NeuralLayer nextLayer = null;
    private int neurons;
    private long[] bias = null;
    private long[][] synapse = null;

    public NeuralLayer(int neurons, ActivationFunction activationFunction) {
        this.neurons = neurons;
        this.activationFunction = activationFunction;
    }

    private void setMatrix(long[] bias, long[] biasLimits, long[][] synapse, long[] synapseLimits) {
        this.bias = MatrixUtils.copy(bias);
        this.biasLimits = MatrixUtils.copy(biasLimits);
        this.synapse = MatrixUtils.copy(synapse);
        this.synapseLimits = MatrixUtils.copy(synapseLimits);
    }

    public void connect(int synapses) {
        if(synapse == null) {
            bias = MatrixUtils.getMatrix(neurons);
            synapse = MatrixUtils.getMatrix(neurons, synapses);
        }
    }

    public void connect(NeuralLayer neuralLayer) {
        nextLayer = neuralLayer;
        neuralLayer.connect(neurons);
    }

    public NeuralLayer getClone() {
        NeuralLayer neuralLayer = new NeuralLayer(neurons, activationFunction);
        neuralLayer.setMatrix(bias, biasLimits, synapse, synapseLimits);
        return neuralLayer;
    }

    public void randomize(int minValue, int maxValue) {
        randomize(minValue, maxValue, minValue/10, maxValue/10);
    }

    public void randomize(int synapseMinValue, int synapseMaxValue, int biasMinValue, int biasMaxValue) {
        if(synapse != null) {
            biasLimits[0] = biasMinValue;
            biasLimits[1] = biasMaxValue;
            synapseLimits[0] = synapseMinValue;
            synapseLimits[1] = synapseMaxValue;

            MatrixUtils.randomize(bias, biasLimits[0], biasLimits[1]);
            MatrixUtils.randomize(synapse, synapseLimits[0], synapseLimits[1]);
        }
    }

    public void mutate(int minValue, int maxValue) {
        if(bias != null) {
            long[] biasMutation = MatrixUtils.getMatrix(bias.length);
            MatrixUtils.randomize(biasMutation, minValue / 10, maxValue / 10);
            bias = MatrixUtils.sum(bias, biasMutation, biasLimits[0], biasLimits[1]);
        }

        if(synapse != null) {
            long[][] synapseMutation = MatrixUtils.getMatrix(synapse.length, synapse[0].length);
            MatrixUtils.randomize(synapseMutation, minValue, maxValue);
            synapse = MatrixUtils.sum(synapse, synapseMutation, synapseLimits[0], synapseLimits[1]);
        }
    }

    public long[] feedForward(long[] input) {
        long[] neuron;

        // Calcula as sinapses para os dados recebidos. Se nao tiver sinapse registrada, indica camada de entrada
        if(synapse != null) {
            long[][] n = MatrixUtils.getMatrix(input.length, 1);
            for(int i=0; i<input.length; i++) {
                n[i][0] = input[i];
            }
//            neuron = MatrixUtils.getColumn(MatrixUtils.multiply(synapse, n), 0);
            neuron = MatrixUtils.sum(MatrixUtils.getColumn(MatrixUtils.multiply(synapse, n), 0), bias);
        } else {
            neuron = input;
        }

        // Executa a funcao de ativacao
        if(activationFunction != null) {
            for (int i = 0; i < neuron.length; i++) {
                neuron[i] = activationFunction.calculate(neuron[i]);
            }
        }

        // Repassa para a proxima camada ou retorna se for a ultima camada
        //MatrixUtils.print(neuron);
        if(nextLayer != null) {
            return nextLayer.feedForward(neuron);
        } else {
            return neuron;
        }
    }

    public void print() {
        System.out.println("neurons:" + neurons);
        System.out.println("bias:");
        if(bias == null) {
            System.out.println("NULO");
        } else {
            MatrixUtils.print(bias);
        }
        System.out.println("synapse:");
        if(synapse == null) {
            System.out.println("NULO");
        } else {
            MatrixUtils.print(synapse);
        }
    }
}
