package br.com.utikawa.rna.utils;

import java.util.Random;

public class MatrixUtils {
    private static Random random = new Random();
    private static double getRandom() {
        return random.nextDouble();
    }

    public static long[] getMatrix(int rows) {
        return new long[rows];
    }

    public static long[] getRandomizedMatrix(int rows, long minValue, long maxValue) {
        return randomize(getMatrix(rows), minValue, maxValue);
    }

    public static long[] randomize(long[] matrix, long minValue, long maxValue) {
        // Random retorna sempre um valor menor que 1 então devemos somar 1 ou o ultimo numero nunca sera retornado.
        long range = maxValue - minValue + 1;

        for(int i=0; i<matrix.length; i++) {
            matrix[i] = (long)(getRandom()*range) + minValue;
        }

        return matrix;
    }

    public static long[][] getMatrix(int rows, int cols) {
        return new long[rows][cols];
    }

    public static long[][] getRandomizedMatrix(int rows, int cols, long minValue, long maxValue) {
        return randomize(getMatrix(rows, cols), minValue, maxValue);
    }

    public static long[][] randomize(long[][] matrix, long minValue, long maxValue) {
        for(int i=0; i<matrix.length; i++) {
            matrix[i] = randomize(matrix[i], minValue, maxValue);
        }

        return matrix;
    }

    public static void print(long[] matrix) {
        System.out.print("[");
        for(long item : matrix) {
            System.out.print(" " + item);
        }
        System.out.println(" ]");
    }

    public static void print(long[][] matrix) {
        for(long[] item : matrix) {
            print(item);
        }
    }

    public static long[] sum(long[] a, long[] b) {
        return sum(a, b, null, null);
    }

    public static long[] sum(long[] a, long[] b, Long minValue, Long maxValue) {
        if(a.length != b.length) {
            throw new IllegalArgumentException("Matrizes devem ser de tamanhos iguais!");
        }

        long[] c = getMatrix(a.length);
        for(int i=0; i<a.length; i++) {
            c[i] = a[i] + b[i];
            c[i] = minValue == null ? c[i] : Math.max(minValue, c[i]);
            c[i] = maxValue == null ? c[i] : Math.min(maxValue, c[i]);
        }

        return c;
    }

    public static long[][] sum(long[][] a, long[][] b) {
        return sum(a, b, null, null);
    }

    public static long[][] sum(long[][] a, long[][] b, Long minValue, Long maxValue) {
        if(a.length != b.length) {
            throw new IllegalArgumentException("Matrizes devem ser de tamanhos iguais!");
        }

        long[][] c = getMatrix(a.length, a[0].length);
        for(int i=0; i<a.length; i++) {
            c[i] = sum(a[i], b[i], minValue, maxValue);
        }

        return c;
    }

    public static long[][] multiply(long[][] a, long[][] b) {
        if(a[0].length != b.length) {
            throw new IllegalArgumentException("Numero de colunas da Matriz A deve ser igual ao numero de linhas da Matriz B!");
        }

        long[][] c = getMatrix(a.length, b[0].length);
        for(int i=0; i<c.length; i++) {
            for(int j=0; j<c[0].length; j++) {
                c[i][j] = 0;
                for(int n=0; n<b.length; n++) {
                    c[i][j] += a[i][n] * b[n][j];
                }
            }
        }

        return c;
    }

    public static long[] getColumn(long[][] matrix, int col) {
        long[] cols = getMatrix(matrix.length);
        for(int i=0; i<matrix.length; i++) {
            cols[i] = matrix[i][col];
        }

        return cols;
    }

    public static long[] copy(long[] src) {
        if(src == null) {
            return src;
        }

        long[] dst = new long[src.length];
        System.arraycopy(src, 0, dst, 0, src.length);

        return dst;
    }

    public static long[][] copy(long[][] src) {
        if(src == null) {
            return src;
        }

        long[][] dst = new long[src.length][src[0].length];
        for(int i=0; i<src.length; i++) {
            dst[i] = copy(src[i]);
        }

        return dst;
    }
}
