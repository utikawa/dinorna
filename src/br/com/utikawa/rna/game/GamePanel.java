package br.com.utikawa.rna.game;

import br.com.utikawa.rna.game.components.Dino;
import br.com.utikawa.rna.game.components.Ground;
import br.com.utikawa.rna.game.components.Obstacles;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

class GamePanel extends JPanel implements KeyListener, Runnable {
    private GameController gameController;

    public static int WIDTH;
    public static int HEIGHT;
    private Thread animator;

    public GamePanel() {
        WIDTH = UserInterface.WIDTH;
        HEIGHT = UserInterface.HEIGHT;
        gameController = new GameController(WIDTH, HEIGHT);

        setSize(WIDTH, HEIGHT);
        setVisible(true);

        animator = new Thread(this);
        animator.start();
    }

    public void paint(Graphics g) {
        super.paint(g);
        gameController.paint(g);
    }

    public void keyTyped(KeyEvent e) {
        // System.out.println(e);
        gameController.processKeyEvent(e);
    }

    public void keyPressed(KeyEvent e) {
        // System.out.println("keyPressed: "+e);
    }

    public void keyReleased(KeyEvent e) {
        // System.out.println("keyReleased: "+e);
    }

    @Override
    public void run() {
        // Loop do jogo. Nunca termina!
        while(true) {
            gameController.getRunner().run();
            repaint();

            try {
                Thread.sleep(15);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}