package br.com.utikawa.rna.game.components;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

public interface IDinoInput {
    enum Input {
        JUMP
    }

    ArrayList<Input> getInputs(ArrayList<KeyEvent> keys, Dino.DinoState dinoState);
}
