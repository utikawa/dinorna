package br.com.utikawa.rna.game.components;

import br.com.utikawa.rna.game.utility.Resource;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Dino {
    private final DinoState dinoState = new DinoState();
    private IDinoInput dinoInput;

    private BufferedImage currentImage;
    private static int jumpFactor = 7;

    public static final int STAND_STILL = 1,
            RUNNING = 2,
            JUMPING = 3,
            DIE = 4;
    private final int LEFT_FOOT = 1,
            RIGHT_FOOT = 2,
            NO_FOOT = 3;

    class DinoState {
        public Obstacles.Obstacle nextObstacle;
        public boolean topPointReached;
        public int dinoBaseY, dinoTopY, dinoStartX, dinoEndX;
        public int dinoTop, topPoint;
        public int state;
        public int foot;
    }

    private final static BufferedImage standDino;
    private final static BufferedImage leftFootDino;
    private final static BufferedImage rightFootDino;
    private final static BufferedImage deadDino;
    static {
        standDino = new Resource().getResourceImage("../images/Dino-stand.png");
        leftFootDino = new Resource().getResourceImage("../images/Dino-left-up.png");
        rightFootDino = new Resource().getResourceImage("../images/Dino-right-up.png");
        deadDino = new Resource().getResourceImage("../images/Dino-big-eyes.png");
    }

    public Dino(IDinoInput dinoInput) {
        dinoState.dinoBaseY = Ground.GROUND_Y + 5;
        dinoState.dinoTop = dinoState.dinoTopY = dinoState.dinoBaseY - standDino.getHeight();
        dinoState.dinoStartX = 100;
        dinoState.dinoEndX = dinoState.dinoStartX + standDino.getWidth();
        dinoState.topPoint = dinoState.dinoTopY - 120;

        dinoState.state = STAND_STILL;
        dinoState.foot = NO_FOOT;

        currentImage = standDino;

        this.dinoInput = dinoInput;
    }

    public IDinoInput getDinoInput() {
        return dinoInput;
    }

    public boolean isAlive() {
        return dinoState.state != DIE;
    }

    public void create(Graphics g) {
        g.drawImage(currentImage, dinoState.dinoStartX, dinoState.dinoTop, null);
    }

    public void die() {
        dinoState.state = DIE;
        currentImage = deadDino;
    }

    public Rectangle getDino() {
        Rectangle dino = new Rectangle();
        dino.x = dinoState.dinoStartX;

        dino.y = dinoState.dinoTop;

        dino.width = standDino.getWidth();
        dino.height = standDino.getHeight();

        return dino;
    }

    public void startRunning() {
        dinoState.dinoTop = dinoState.dinoTopY;
        dinoState.state = RUNNING;
    }

    public void jump() {
        if(dinoState.state != JUMPING) {
            dinoState.dinoTop = dinoState.dinoTopY;
            dinoState.topPointReached = false;
            dinoState.state = JUMPING;
        }
    }

    public void update(ArrayList<KeyEvent> keys, Obstacles.Obstacle nextObstacle) {
        if(dinoState.state != DIE) {
            dinoState.nextObstacle = nextObstacle;
            if (dinoInput != null) {
                for (IDinoInput.Input item : dinoInput.getInputs(keys, dinoState)) {
                    if (item.equals(IDinoInput.Input.JUMP)) {
                        jump();
                    }
                }
            }
        }

        switch (dinoState.state) {
            case STAND_STILL:
                System.out.println("stand");
                currentImage = standDino;
                break;

            case RUNNING:
                if (dinoState.foot == NO_FOOT) {
                    dinoState.foot = LEFT_FOOT;
                    currentImage = leftFootDino;
                } else if (dinoState.foot == LEFT_FOOT) {
                    dinoState.foot = RIGHT_FOOT;
                    currentImage = rightFootDino;
                } else {
                    dinoState.foot = LEFT_FOOT;
                    currentImage = leftFootDino;
                }
                break;

            case JUMPING:
                currentImage = standDino;
                if (dinoState.dinoTop > dinoState.topPoint && !dinoState.topPointReached) {
                    dinoState.dinoTop -= jumpFactor;
                } else if (dinoState.dinoTop <= dinoState.topPoint && !dinoState.topPointReached) {
                    dinoState.topPointReached = true;
                    dinoState.dinoTop += jumpFactor;
                } else {
                    dinoState.dinoTop += jumpFactor;
                    if (dinoState.dinoTopY == dinoState.dinoTop) {
                        dinoState.state = RUNNING;
                        dinoState.topPointReached = false;
                    }
                }
                break;
            case DIE:
                currentImage = deadDino;
                dinoState.dinoStartX -= 4;
            break;
        }
    }
}