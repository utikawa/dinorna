package br.com.utikawa.rna.game.components;

import br.com.utikawa.rna.core.NeuralNetwork;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class DinoInputRNA implements IDinoInput {
    private NeuralNetwork neuralNetwork;

    public DinoInputRNA(NeuralNetwork neuralNetwork) {
        this.neuralNetwork = neuralNetwork;
    }

    public NeuralNetwork getNeuralNetwork() {
        return neuralNetwork;
    }

    @Override
    public ArrayList<Input> getInputs(ArrayList<KeyEvent> keys, Dino.DinoState dinoState) {
        ArrayList<Input> inputs = new ArrayList<>();
        //System.out.println("Iniciando. Distancia: " + dinoState.nextObstacle.x);
        long l = neuralNetwork.feedForward(new long[]{dinoState.nextObstacle.x, dinoState.nextObstacle.y})[0];
        //System.out.println("Resultado: " + l);
        if(l > 0) {
            inputs.add(Input.JUMP);
        }

        return inputs;
    }
}
