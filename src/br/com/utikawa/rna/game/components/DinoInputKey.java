package br.com.utikawa.rna.game.components;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class DinoInputKey implements IDinoInput {
    @Override
    public ArrayList<Input> getInputs(ArrayList<KeyEvent> keys, Dino.DinoState dinoState) {
        ArrayList<Input> inputs = new ArrayList<>();
        for(KeyEvent key : keys) {
            if(key.getKeyChar() == ' ') {
                inputs.add(Input.JUMP);
            }
        }

        return inputs;
    }
}
