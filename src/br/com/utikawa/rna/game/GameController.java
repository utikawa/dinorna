package br.com.utikawa.rna.game;

import br.com.utikawa.rna.core.NeuralNetwork;
import br.com.utikawa.rna.game.components.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class GameController {
    private Dino bestDino = null;
    private final ArrayList<KeyEvent> currentKeys = new ArrayList<>();
    private Runnable runner;
    private int width;
    private int height;

    public enum GameState {
        STOPPED, RUNNING, GAME_OVER
    }
    private GameState gameState = GameState.STOPPED;

    private Ground ground;
    private final ArrayList<Dino> dinoList = new ArrayList<>();
    private Obstacles obstacles;

    private int score;

    GameController(int width, int height) {
        this.width = width;
        this.height = height;

        ground = new Ground(height);
        createDinos();
        obstacles = new Obstacles((int) (width * 1.5));

        setGameState(GameState.STOPPED);
    }

    public Runnable getRunner() {
        if(runner == null) {
            runner = new Runnable() {
                @Override
                public void run() {
                    if (gameState.equals(GameState.RUNNING)) {
                        updateGame();
                    }
                }
            };
        }

        return runner;
    }

    private ArrayList<NeuralNetwork> getRNAs(NeuralNetwork src) {
        int rnaQty = 5000;

        boolean randomizeAll = false;
        if(src == null) {
            randomizeAll = true;
            src = new NeuralNetwork(2, new int[]{6,4,4}, 1);
            src.randomize(-1000, 1000);
        }

        ArrayList<NeuralNetwork> rnaArray = new ArrayList<>();
        rnaArray.add(src); // Mantem o melhor
        for(int i=1; i < rnaQty; i++) {
            NeuralNetwork current = src.getClone();
            if(randomizeAll) {
                current.randomize(-1000, 1000);
            } else {
                current.mutate(-100, 100);
            }
            rnaArray.add(current);
        }

        return rnaArray;
    }

    private void drawBoard(Graphics g) {
        if(!gameState.equals(GameState.STOPPED)) {
            g.setFont(new Font("Courier New", Font.BOLD, 25));
            g.drawString(Integer.toString(score), width / 2 - 5, 100);
            g.drawString("Vivos: " + getDinoAliveQuantity(), 50, 30);
        }
    }

    private void setGameState(GameState gameState) {
        this.gameState = gameState;
        if(gameState.equals(GameState.RUNNING)) {
            score = 0;
            obstacles.resume();
            createDinos();

            System.out.println("Game starts");
            for (Dino dino : dinoList) {
                dino.startRunning();
            }
        }
    }

    public void paint(Graphics g) {
        drawBoard(g);

        ground.create(g);
        for (Dino dino : dinoList) {
            dino.create(g);
        }
        obstacles.create(g);
    }

    public void createDinos() {
        NeuralNetwork neuralNetwork = null;
        if(bestDino != null) {
            IDinoInput dinoInput = bestDino.getDinoInput();
            if (dinoInput instanceof DinoInputRNA) {
                neuralNetwork = ((DinoInputRNA) bestDino.getDinoInput()).getNeuralNetwork();
            }
            bestDino = null;
        }

        synchronized (dinoList) {
            dinoList.clear();
            ArrayList<NeuralNetwork> rnaList = getRNAs(neuralNetwork);

            dinoList.add(new Dino(new DinoInputKey()));
            for (NeuralNetwork item : rnaList) {
                dinoList.add(new Dino(new DinoInputRNA(item)));
            }
        }
    }

    private boolean isDinoAlive() {
        for (Dino dino : dinoList) {
            if (dino.isAlive()) {
                return true;
            }
        }
        return false;
    }

    private int getDinoAliveQuantity() {
        int alive = 0;
        for (Dino dino : dinoList) {
            if (dino.isAlive()) {
                alive++;
            }
        }

        return alive;
    }

    public void updateGame() {
        score += 1;

        synchronized (currentKeys) {
            ground.update();
            obstacles.update();
            synchronized (dinoList) {
                for (Dino dino : dinoList) {
                    dino.update(currentKeys, obstacles.getNextObstacle());
                }

                for (Dino dino : dinoList) {
                    if (dino.isAlive() && obstacles.hasCollided(dino)) {
                        dino.die();
                        if (!isDinoAlive()) {
                            bestDino = dino;
                            setGameState(GameState.GAME_OVER);
                            break;
                        }
                        System.out.println("collide");
                    }
                }
            }
            // game complete condition
            currentKeys.clear();
        }
        if(gameState.equals(GameState.GAME_OVER)) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            setGameState(GameState.RUNNING);
        }
    }

    public void processKeyEvent(KeyEvent e) {
        if (e.getKeyChar() == 'x') {
            setGameState(GameState.GAME_OVER);
        } else if (e.getKeyChar() == ' ') {
            if(gameState.equals(GameState.RUNNING)) {
                synchronized (currentKeys) {
                    currentKeys.add(e);
                }
            } else {
                setGameState(GameState.RUNNING);
            }
        }
    }
}
