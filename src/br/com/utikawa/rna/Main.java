package br.com.utikawa.rna;

import br.com.utikawa.rna.game.UserInterface;

public class Main {
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new UserInterface().createAndShowGUI();
            }
        });
    }
}
